
/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Name: common.h , common.c header
 *
 * Copyright (C) 2019, Moises Torres Aguilar
 *
 *****************************************************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>

typedef struct {
  char *dataset;
  size_t width, height, tmax;
  double error_max;
  int nhd_fun;

} t_options;

int	get_options(t_options *opt, int nb_options, char **options);
int	count_numeric_headers(const char*first_line);
size_t	count_lines(const char *str);
char	*file2str(const char *filename);
int	index_label(char ***labels, char *label);
#endif /* __COMMON_H__ */
