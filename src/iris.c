
/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Name: iris.c - Uses kohonen network to train data and display the map with labels
 *
 * Copyright (C) 2019, Moises Torres Aguilar
 *
 *****************************************************************************/

#include "common.h"
#include "kohonen.h"


/// Prints programme's usage text
void print_usage() {
  printf("Usage: ./kohonen -d FILE [-w INT -h INT -c INT -e DOUBLE [--gaussian | --mexican-hat]]\n");
  printf("\t -d, --dataset\t\tPath to file of dataset\n");
  printf("\t -w, --width\t\tMap width (> 2). Default is 10.\n");
  printf("\t -h, --height\t\tMap height (>2). Default is 10.\n");
  printf("\t -c, --tmax\t\tTraining maximal iteration (>0). Default is 1000.\n");
  printf("\t -e, --error\t\tStopping condition on weigt update (positive). Default is 10E-7.\n");
  printf("\t --gaussian\t\tSets a gaussian function as neighborhood function.\n");
  printf("\t --mexican-hat\t\tSets a mexican-hat function as neighborhood function.\n");

}

int free_neurons(t_neuron **neurons, size_t w, size_t h) {
  size_t i, j;
  
  if (!neurons)
    return 0;

  for (i = 0; i < w; ++i) {
    for (j = 0; j < h; ++j) {
      if (neurons[i][j].d)
	free(neurons[i][j].d);
    }
    free(neurons[i]);
  }
  free(neurons);
  
  return 1;
}

void print_data(t_training_data const *data, size_t size) {
  for (size_t i = 0; i < size; ++i)
    for (size_t j = 0; j < data[i].size; ++j)
      printf("%.1f,", data[i].d[j]);
}

int main(int argc, char **argv) {
  char *content;
  t_training_data *data;
  t_som map;
  size_t i, nb_data;
  int error_init;
  t_options opt;

  // Default options  
  opt = (t_options) {"", 10, 10, 1000, 0.0000001, MEXICAN_HAT};
  
  // Read options with getopt
  error_init = get_options(&opt, argc, argv);
  if (!error_init) {
    print_usage();
    return 1;
  }
  
  // Read file
  content = file2str(opt.dataset);
  //printf("%s", content);
  if (!content)
    return 1;
  
  // Count number of data inputs
  nb_data = count_lines(content);
  //printf("%lu\n", nb_data);
  
  if (nb_data == 0) {
    printf("File is empty! Bye.\n");
    free(content);
    return 1;
  }
  
  data = (t_training_data*) malloc(sizeof(t_training_data) * nb_data);
  if (!data) {
    printf("Error while allocating the training data.\n");
    goto free_all;
  }

  //Initialise data
  error_init = read_data(data, content);
  if (!error_init) {
    printf("Error while reading the training data.\n");
    free(content);
    return 1;
  }
  //print_data(data, nb_data);

  // Map initialisation  
  error_init = initialise(&map, data[0].size, opt.width, opt.height, opt.tmax);
  if (!error_init) {    
    
    // Data normalisation
    normalise_dataset(data, nb_data);
    
    // Training
    train_network(&map, data, nb_data, opt.nhd_fun, opt.error_max);

    // Map labeling
    label_network(&map, data, nb_data);

    // Map displaying
    display_network(map);
    
  } else {
    printf("Error while initialising the map.\n");
  }

 free_all:

  if (content)
    free(content);

  if (_labels)
    free(_labels);
  
  if (map.neurons)
    free_neurons(map.neurons, map.width, map.height);
  
  if (data) {
    for (i = 0; i < data->size; ++i)
      if (data[i].d)
  	free(data[i].d);
    free(data);
  }

  return 0;
}
