
/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Name: kohonen.h - kohonen.c header
 *
 * Copyright (C) 2019, Moises Torres Aguilar
 *
 *****************************************************************************/

#ifndef __KOHONEN_H__
#define __KOHONEN_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <float.h>

#include "common.h"

/* Structs */

typedef struct {
  double *d;
  size_t size;
  int label;
  
} t_neuron;

typedef struct {
  double *d;
  size_t size;
  int label;
  
} t_training_data;

typedef struct {
  t_neuron **neurons;
  size_t width, height, tmax;
  double learn_coeff_init, map_radius_init;
  
} t_som;

typedef struct {
  size_t x, y;
  
} t_point;

/* Enums */

enum nhd_fun {GAUSS, MEXICAN_HAT};

/* Global variables */
char ** _labels;

/* Functions provided */

void	train_network(t_som *map, t_training_data const	*data, size_t const nb_data,
		      int const nhd_fun, double const max_error);
int	read_data(t_training_data *data, char* content);
int	initialise(t_som *map, size_t dimension, size_t width,
		   size_t height, size_t const iter_max);
void	normalise_dataset(t_training_data *data, size_t const size);
void	label_network (t_som *map, t_training_data const *data, size_t const nb_data);
void	display_network(t_som map);

#endif /* __KOHONEN_H__ */
