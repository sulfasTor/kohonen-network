
/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Name: common.c - Common functions
 *
 * Copyright (C) 2019, Moises Torres Aguilar
 *
 *****************************************************************************/

#include "common.h"

int get_options(t_options *opt, int nb_options, char **options) {
  int c, dataset_set;
  size_t temp;
  static int gauss_flag;

  dataset_set = 0;
  gauss_flag = 1;
  while (1) {
    static struct option long_options[] =
      {
       {"gaussian", no_argument, &gauss_flag, 1},
       {"mexican-hat", no_argument, &gauss_flag, 0},
       {"dataset", required_argument, 0, 'd'},
       {"width", required_argument, 0, 'w'},
       {"height", required_argument, 0, 'h'},
       {"tmax", required_argument, 0, 'c'},
       {"error", required_argument, 0, 'e'},
       {0, 0, 0, 0}
      };

    int option_index = 0;
    c = getopt_long (nb_options, options, "d:w:h:c:e:",
		     long_options, &option_index);

    if (c == -1)
      break;

    switch (c)
      {
      case 0:
	break;
	
      case 'd':
	opt->dataset = optarg;
	dataset_set = 1;
	break;

      case 'w':
	temp = strtoul(optarg, NULL, 0);
	if (temp < 2)
	  return 0;
	opt->width = temp;
	break;

      case 'h':
	temp = strtoul(optarg, NULL, 0);
	if (temp < 2)
	  return 0;
	opt->height = temp;
	break;
	
      case 'c':
	if (atoi(optarg) < 1)
	  return 0;
	opt->tmax = strtoul(optarg, NULL, 0);
	break;

      case 'e':
	if (!optarg && atof(optarg) < 1)
	  return 0;
	opt->error_max = atof(optarg);
	break;

      default:
	return 0;
      }
  }
  
  if (gauss_flag)
    opt->nhd_fun = 0;
  else
    opt->nhd_fun = 1;
  
  return dataset_set;
}

// Counts commas until a non digit is found
int count_numeric_headers(const char *first_line) {
  int i, counter;

  if (first_line == NULL)
    return 0;
  
  counter = 0;
  i = 0;
  while ((isdigit(first_line[i]) || first_line[i] == ',' ||
	  first_line[i] == '.') && first_line[i] != '\n' && first_line[i] != '\0') {
    if (first_line[i] == ',')
      counter++;
    i++;
  }
  return (counter);
}

size_t count_lines(const char *str) {
  size_t lines, i;
  int c, lc;

  i = 0;
  c = str[i];
  lc = '\n';
  lines = 0;
  while (c && c != EOF) {
    if (c == '\n'  &&  lc != '\n')
      lines++;
    lc = c;
    c = str[i++];
  }
  if (lc != '\n')
    lines++;

  return lines;
}

char *file2str(const char *filename) {
  FILE *fd;
  size_t fsize;
  char *content;

  if (!filename)
    return NULL;

  fd = fopen(filename, "r");
  if (!fd)
    return NULL;
  fseek(fd, 0L, SEEK_END);
  fsize = ftell(fd);
  if (!fsize) {
    fclose(fd);
    return NULL;
  }    
  rewind(fd);
  
  content = (char*)malloc(fsize);
  if (content)
    fread(content, 1, fsize, fd);

  fclose(fd);
  
  return content;
}

int index_label(char ***labels, char *label) {
  int i;

  if (!*labels) {
    *labels = (char**)malloc(2 * sizeof(char*));
    if (!*labels)
      return -1;
    (*labels)[0] = label;
    (*labels)[1] = NULL;
    return 0;
  }
  
  for (i = 0; (*labels)[i] != NULL; ++i)
    if (!strcmp(label, (*labels)[i]))
      return i;

  *labels = realloc(*labels, (i+2) * sizeof(char*));
  (*labels)[i] = label;
  (*labels)[i+1] = NULL;

  return i;
}
