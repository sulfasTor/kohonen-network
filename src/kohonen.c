
/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Name: kohonen.c - Implements kohonen network.
 *
 * Copyright (C) 2019, Moises Torres Aguilar
 *
 *****************************************************************************/

#include "kohonen.h"

int free_ptr_if_fails(t_neuron **ptr, size_t n) {
  if (ptr)
    return 0;
  while(n)
    free(ptr[n--]);
  free(ptr);
  return 1;
}

double euc_dist_2d(double const *d1, double const *d2, size_t const n) {
  double dist = 0.0;

  for (size_t i = 0; i < n; ++i) {
    //printf("(%f - %f)^2 = %f\n", d1[i], d2[i], dist);
    dist += (d1[i] - d2[i]) * (d1[i] - d2[i]);
  }
  return sqrt(dist);
}

double compute_learning_coeff(size_t tmax, size_t curr_iter, double learning_coeff) {
  return (learning_coeff * exp(-(double) curr_iter / tmax));
}

double compute_nhd_radius(size_t tmax, size_t curr_iter, double nhd_radius_init) {
  double time_constant;

  // Implies nhd_radius_init is always greater than 1 and tmax is different of 0
  time_constant = tmax / log(nhd_radius_init);
  return (nhd_radius_init * exp(-(double) curr_iter / time_constant));
}

double compute_strength_gauss(double const alpha, double const sigma, double diff_from_bmu) {
  return (alpha * exp(-diff_from_bmu/(2.0 * sigma * sigma)));
}

double compute_strength_mexican_hat(double const alpha, double const sigma, double diff_from_bmu) {
  return (alpha * (1 - diff_from_bmu / (sigma * sigma)) * exp(-diff_from_bmu / (2.0 * sigma * sigma)));
}

double compute_strength(int nhd_fun, double const alpha, double const sigma, double diff_from_bmu) {
  switch(nhd_fun) {
  case GAUSS:
    return compute_strength_gauss(alpha, sigma, diff_from_bmu);
  case MEXICAN_HAT:
      return compute_strength_mexican_hat(alpha, sigma, diff_from_bmu);
  }

  return compute_strength_gauss(alpha, sigma, diff_from_bmu);
}

t_point find_bmu(t_som map, t_training_data const data) {
  double dist, best_dist;
  size_t i, j, idx, idy;

  best_dist = DBL_MAX;
  idx = 0;
  idy = 0;
  for (i = 0; i < map.width; ++i) {
    for (j = 0; j < map.height; ++j) {
      dist = euc_dist_2d(map.neurons[i][j].d, data.d, data.size);
      if (dist < best_dist) {
	idx = i;
	idy = j;
	best_dist = dist;
      }
    }
  }
  return (t_point) {idx, idy};
}

double update_weights(t_som *map, t_training_data const data, t_point const bmu,
		      double const nhd_radius, double const learn_coeff, int const nhd_fun) {
  size_t i, j, k, start_x, end_x, start_y, end_y;
  size_t nhd_radius_int, diff_from_bmu;
  double delta_w, strength, error;

  nhd_radius_int = (size_t) floor(nhd_radius);
  //printf("bmu: (%lu, %lu) \t\t\t\t\t\t\t\t\talpha=%f, sigma=%lu\n", bmu.x, bmu.y, learn_coeff, nhd_radius_int);

  /// Creates a squared bound
  start_x = ((int)bmu.x - (int)nhd_radius_int < 0 ? 0 : bmu.x - nhd_radius_int);
  end_x = (bmu.x + nhd_radius_int >= map->width ? map->width : bmu.x + nhd_radius_int);
  start_y = ((int)bmu.y - (int)nhd_radius_int < 0 ? 0 : bmu.y - nhd_radius_int);
  end_y = (bmu.y + nhd_radius_int >= map->height ? map->height : bmu.y + nhd_radius_int);

  //printf("x: %lu to %lu\n", start_x, end_x);
  //printf("y: %lu to %lu\n", start_y, end_y);

  error = 0.0;
  for (i = start_x; i < end_x; ++i) {
    for (j = start_y; j < end_y; ++j) {
      diff_from_bmu = (bmu.x-i) * (bmu.x-i) + (bmu.y-j) * (bmu.y-j);
      if (diff_from_bmu <= nhd_radius_int * nhd_radius_int) {
	strength = compute_strength(nhd_fun, learn_coeff, (double) nhd_radius_int, (double) diff_from_bmu);
	//printf("(%lu, %lu) d_bmu=%lu, strength= %f\n", i, j, diff_from_bmu, strength);
	for (k = 0; k < map->neurons[0][0].size; ++ k) {
	  delta_w =  strength * (data.d[k] - map->neurons[i][j].d[k]);
	  map->neurons[i][j].d[k] += delta_w;
	  error += delta_w;
	}
      }
    }
  }
  return fabs(error / (double)map->neurons[0][0].size);
}

double train_input(t_som *map, t_training_data const data, size_t curr_iter, int const nhd_fun) {
  t_point bmu_coord;
  double nhd_radius, learning_coeff, error;

  bmu_coord = find_bmu(*map, data);
  nhd_radius = compute_nhd_radius(map->tmax, curr_iter, map->map_radius_init);
  learning_coeff = compute_learning_coeff(map->tmax, curr_iter, map->learn_coeff_init);
  error = update_weights(map, data, bmu_coord, nhd_radius, learning_coeff, nhd_fun);

  return (error/(double)(map->width * map->height));
}

void train_network(t_som *map, t_training_data const *data, size_t const nb_data, int const nhd_fun, double const error_max) {
  size_t iter;
  double error;

  iter = 0;
  error = DBL_MAX;
  srand(time(0));
  while (error > error_max && map->tmax > iter) {
    error = train_input(map, data[rand() % nb_data], iter, nhd_fun);
    iter++;
  }
}

int read_data(t_training_data *data, char* content) {
  char *token;
  int i, j, numeric_size;

  i = 0;
  numeric_size = count_numeric_headers(content);
  if (numeric_size == 0)
    return 0;

  token = strtok(content, ",\n");
  while (token != NULL) {
    if (isdigit(token[0])) {
      data[i].d = (double*)malloc(numeric_size * sizeof(double));
      j = 0;
      data[i].d[j++] = atof(token);
      while (j < numeric_size)
	data[i].d[j++] = atof(strtok(NULL, ",\n"));
      data[i].size = numeric_size;
    } else {
      data[i++].label = index_label(&_labels, token);
    }
    token = strtok(NULL, ",\n");
  }

  if (token)
    free(token);
  
  if (!data)
    return 0;
  return 1;
}

void normalise_dataset(t_training_data *data, size_t size) {
  double norm;
  size_t i, j;

  for (j = 0; j < data->size; ++j) {
    norm = 0.0;
    for (i = 0; i < size; ++i)
      norm += data[i].d[j] * data[i].d[j];
    norm = sqrt(norm);

    for (i = 0; i < size; ++i)
      data[i].d[j] /= norm;
  }
}

double *initialise_weights(size_t n) {
  size_t i;
  double *w;

  w = (double *) malloc(n * sizeof(double));
  for (i = 0; i < n; ++i) {
    w[i] = (double)rand() / (double)RAND_MAX;;
  }

  return w;
}

/// Randomly initialize map
int initialise(t_som *map, size_t dimension,
	       size_t width, size_t height, size_t iter_max) {
  size_t i, j;

  srand(time(0));
  map->tmax = iter_max;							/// Set tmax
  map->width = width;							/// Set map width
  map->height = height;							/// Set map height
  map->learn_coeff_init = (iter_max - 1) / (double)iter_max;		/// Set alpha, learning coefficient
  map->map_radius_init = fmax((double)width, (double)height) / 2.0;	/// Set sigma, neighborhood coefficient

  map->neurons = (t_neuron**)malloc(width * sizeof(t_neuron*));
  if (!map->neurons) {
    free(map);
    printf("Error while allocation the neurons.\n");
    return 1;
  }

  /// Fill map with random weights
  for (i = 0; i < width; ++i) {
    map->neurons[i] = (t_neuron*)malloc(height * sizeof(t_neuron));
    if (free_ptr_if_fails(map->neurons, i)) {
      printf("Error while allocation the node (%lu)", i);
      return 1;
    }
    for (j = 0; j < height; ++j) {
      map->neurons[i][j].d = initialise_weights(dimension);
      map->neurons[i][j].size = dimension;
    }
  }

  return 0;
}

void label_network (t_som *map, t_training_data const *data, size_t const nb_data) {
  size_t i, j, k, bmu;
  double dist, best_dist;

  for (i = 0; i < map->width; ++i) {
    for (j = 0; j < map->height; ++j) {
      bmu = 0;
      best_dist = DBL_MAX;
      dist = 0.0;
      for (k = 0; k < nb_data; ++k) {
	dist = euc_dist_2d(map->neurons[i][j].d, data[k].d, data[k].size);
	if (dist < best_dist) {
	  best_dist = dist;
	  bmu = k;
	}
      }
      map->neurons[i][j].label = data[bmu].label;
    }
  }
}

void display_network(t_som map)
{
  int l;
  size_t i, j;

  for (l = 0; _labels[l] != NULL; ++l)
    printf("\033[48;5;%dm%s\033[49m\n", l + 1, _labels[l]);

  printf("\n");
  for (i = 0; i < map.width; ++i) {
    for (j = 0; j < map.height; ++j)
      printf("\033[48;5;%dm  ", map.neurons[i][j].label + 1);
    puts("\033[49m");
  }
}
