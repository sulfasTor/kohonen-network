SOURCES = $(wildcard src/*.c)
# HEADERS = $(wildcard src/*.h)

OBJECTS = $(SOURCES:%.c=%.o)
PROGRAM = kohonen

CC := $(shell which gcc || which clang)
CFLAGS = -Wall -Wextra -Werror -O3
LIBS = m
LDFLAGS = $(LIBS:%=-l%)

all: $(PROGRAM)

$(PROGRAM): $(OBJECTS)
	$(CC) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY : clean
clean :
	rm -f $(PROGRAM) $(OBJECTS)

re: clean all
