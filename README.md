

# SOM Neural Network Iris

This is my SOM implementation in C. It was designed to read any
dataset using the column model as in the iris dataset. Another dataset
can be used from the directory `training_data`.

Self-Organizing Map is an unsupervisedly trained artificial neural
network that produces a two-dimensional representation of input space
of training samples.


# Building

    make


# Execution

    Usage: ./kohonen -d FILE [-w INT -h INT -c INT -e DOUBLE [--gaussian | --mexican-hat]]
    	 -d, --dataset		Path to file of dataset
    	 -w, --width		Map width (> 2). Default is 10.
    	 -h, --height		Map height (>2). Default is 10.
    	 -c, --tmax		Training maximal iteration (>0). Default is 1000.
    	 -e, --error		Stopping condition on weigt update (positive). Default is 10E-7.
    	 --gaussian		Sets a gaussian function as neighborhood function.
    	 --mexican-hat		Sets a mexican-hat function as neighborhood function.


## Example

    ./kohonen -d training_data/iris.data -c 5000 --mexican-hat


## Output

![img](iris-2.png)
Coloring works on most terminals. Tested on `Terminator` and `Xfce
Terminal`.

